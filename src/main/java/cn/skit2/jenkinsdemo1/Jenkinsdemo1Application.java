package cn.skit2.jenkinsdemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jenkinsdemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(Jenkinsdemo1Application.class, args);
    }

}
