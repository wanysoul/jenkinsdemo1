package cn.skit2.jenkinsdemo1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Jenkinsdemo1ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
